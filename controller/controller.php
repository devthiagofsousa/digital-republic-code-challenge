<?php

require_once("global-controller.php");


//Tira a virgula e troca por ponto, e Tranforma a variavel em Float
function formatar_numero ($numero){

    if(!empty($numero)){

        $numero_formatado = str_replace(',', '.', $numero); 

        $numero_formatado = floatval($numero_formatado);

        return $numero_formatado;

    }else{

        return $numero;

    }
    

}

function qtd_baldes ($larguras, $alturas, $portas = 0, $janelas = 0){

    //Tipos de Balde de Tinta
    $balde_18 = 18;
    $balde_3_6 = 3.6;
    $balde_2_5 = 2.5;
    $balde_0_5 = 0.5;
    
    $i_balde_18 = 0;
    $i_balde_3_6 = 0;
    $i_balde_2_5 = 0;
    $i_balde_0_5 = 0;

    $qtd_baldes = array();

    $area_tinta = 0;

    //Area das portas
    $area_porta = 0.8 * 1.9;
    $area_portas = $area_porta * $portas;

    //Area das Janelas
    $area_janela = 2.0 * 1.2;
    $area_janelas = $area_janela * $janelas;

    //Area das Paredes
    $area_paredes = $larguras * $alturas;
    $area_paredes -=  $area_portas;
    $area_paredes -=  $area_janelas;

    //Calcula a Quantidade de Baldes
    while ($area_paredes != $area_tinta) {

        if($area_paredes - $area_tinta > $balde_18){

            $area_tinta += $balde_18;

            $i_balde_18++;

            $qtd_baldes["18l"] = $i_balde_18;

        }elseif($area_paredes - $area_tinta > $balde_3_6){

            $area_tinta += $balde_3_6;

            $i_balde_3_6++;

            $qtd_baldes["3l"] = $i_balde_3_6;

        }elseif($area_paredes - $area_tinta > $balde_2_5){

            $area_tinta += $balde_2_5;

            $i_balde_2_5++;

            $qtd_baldes["2l"] = $i_balde_2_5;

        }
        elseif($area_paredes - $area_tinta > $balde_0_5){

            $area_tinta += $balde_0_5;

            $i_balde_0_5++;

            $qtd_baldes["0_5l"] = $i_balde_0_5;

        }else{

            $area_tinta = $area_paredes;

            $i_balde_0_5++;

            $qtd_baldes["0_5l"] = $i_balde_0_5;
        }
    }

    return $qtd_baldes;

}

//Pega Altura das Paredes
$altura_parede_01 = htmlspecialchars(formatar_numero($_POST["altura-parede-01"]));
$altura_parede_02 = htmlspecialchars(formatar_numero($_POST["altura-parede-02"]));
$altura_parede_03 = htmlspecialchars(formatar_numero($_POST["altura-parede-03"]));
$altura_parede_04 = htmlspecialchars(formatar_numero($_POST["altura-parede-04"]));

//Pega Largura das Paredes
$largura_parede_01 = htmlspecialchars(formatar_numero($_POST["largura-parede-01"]));
$largura_parede_02 = htmlspecialchars(formatar_numero($_POST["largura-parede-02"]));
$largura_parede_03 = htmlspecialchars(formatar_numero($_POST["largura-parede-04"]));
$largura_parede_04 = htmlspecialchars(formatar_numero($_POST["largura-parede-04"]));

//Pega Possui Porta
$possui_portas_01 = htmlspecialchars($_POST["possui-portas-01"]);
$possui_portas_02 = htmlspecialchars($_POST["possui-portas-02"]);
$possui_portas_03 = htmlspecialchars($_POST["possui-portas-03"]);
$possui_portas_04 = htmlspecialchars($_POST["possui-portas-04"]);

//Pega Possui Janela
$possui_janelas_01 = htmlspecialchars($_POST["possui-janelas-01"]);
$possui_janelas_02 = htmlspecialchars($_POST["possui-janelas-02"]);
$possui_janelas_03 = htmlspecialchars($_POST["possui-janelas-03"]);
$possui_janelas_04 = htmlspecialchars($_POST["possui-janelas-04"]);

//Pega Quantidade de Portas
$qtd_portas_01 = 0;
$qtd_portas_02 = 0;
$qtd_portas_03 = 0;
$qtd_portas_04 = 0;

if(!empty($possui_portas_01)){

    $qtd_portas_01 = htmlspecialchars(intval($_POST["qtd-portas-01"]));

}

if(!empty($possui_portas_02)){

    $qtd_portas_02 = htmlspecialchars(intval($_POST["qtd-portas-02"]));

}

if(!empty($possui_portas_03)){

    $qtd_portas_03 = htmlspecialchars(intval($_POST["qtd-portas-03"]));

}

if(!empty($possui_portas_04)){

    $qtd_portas_04 = htmlspecialchars(intval($_POST["qtd-portas-04"]));

}

//Pega Quantidade de Janelas
$qtd_janelas_01 = 0;
$qtd_janelas_02 = 0;
$qtd_janelas_03 = 0;
$qtd_janelas_04 = 0;

if(!empty($possui_janelas_01)){

    $qtd_janelas_01 = htmlspecialchars(intval($_POST["qtd-janelas-01"]));

}

if(!empty($possui_janelas_02)){

    $qtd_janelas_02 = htmlspecialchars(intval($_POST["qtd-janelas-02"]));

}

if(!empty($possui_janelas_03)){

    $qtd_janelas_03 = htmlspecialchars(intval($_POST["qtd-janelas-03"]));

}

if(!empty($possui_janelas_04)){

    $qtd_janelas_04 = htmlspecialchars(intval($_POST["qtd-janelas-04"]));

}


//Soma das Larguras
$largura_paredes = $largura_parede_01 + $largura_parede_02 + $largura_parede_03 + $largura_parede_04;

//Soma das Alturas
$altura_paredes = $altura_parede_01 + $altura_parede_02 + $altura_parede_03 + $altura_parede_04;

//Soma das Qtd de Portas
$qtd_portas = $qtd_portas_01 + $qtd_portas_02 + $qtd_portas_03 + $qtd_portas_04;

//Soma das Janelas
$qtd_janelas = $qtd_janelas_01 + $qtd_janelas_02 + $qtd_janelas_03 + $qtd_janelas_04;

$baldes = qtd_baldes($largura_paredes, $altura_paredes, $qtd_portas, $qtd_janelas);

if(!empty($baldes)){

    $url = url();

    //Pagina do Resultado
    $url_resultado = $url."resultado.php?qtd_baldes=".urldecode(serialize($baldes));

    header("Location: ".$url_resultado);

    die();

}else{

    $url = url();

    header("Location: ".$url);

    die();

}

?>