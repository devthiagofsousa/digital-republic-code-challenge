<?php

//Header do Sistema
require_once( "view" . DIRECTORY_SEPARATOR . "includes". DIRECTORY_SEPARATOR . "menu.php");

$resultado = unserialize(urldecode($_GET["qtd_baldes"]));

if(!empty($resultado)){
?>
    <section class="calculadora">

        <div class="calculadora-container resultado">

            <h1>Resultado</h1>

            <p> São Necessarios:
                <ul>
                    <?php
                        echo(isset($resultado["18l"]) ? "<b><li>".$resultado["18l"]." Baldes de 18L</b></li>" : "");
                        echo(isset($resultado["3l"]) ? "<b><li>".$resultado["3l"]." Baldes de 3,6L</b></li>" : "");
                        echo(isset($resultado["2l"]) ? "<b><li>".$resultado["2l"]." Baldes de 2,5L</b></li>" : "");
                        echo(isset($resultado["0_5l"]) ? "<b><li>".$resultado["0_5l"]." Baldes de 0,5L</b></li>" : "");
                    ?>
                </ul>
            </p>
            <a href="<?php echo url(); ?>">Voltar para Página Inciar</a>
        </div>

    </section>
<?php
}else{
    $url = url();

    header("Location: ".$url);

    die();
}

//Rodapé do Sistema
require_once( "view" . DIRECTORY_SEPARATOR . "includes". DIRECTORY_SEPARATOR . "rodape.php");

?>