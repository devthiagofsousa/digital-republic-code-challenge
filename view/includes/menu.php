<?php 

$base_dir = str_replace(realpath(dirname(__FILE__)), '', realpath(dirname(__FILE__) . ''.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'));

require_once($base_dir. DIRECTORY_SEPARATOR ."controller". DIRECTORY_SEPARATOR . "global-controller.php");

?>

<!doctype html>
<html lang="pt-BR">
	<head>
		<meta charset="UTF-8">
		<title>Calculadora de Quantidade de Tinta</title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Sistema que ajuda o usuário a calcular a quantidade de tinta necessária para pintar uma sala">

		<link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="<?php echo url(); ?>view/css/style.css">

	</head>
	<body>

		<main>