jQuery(document).ready(function() {


    //Retira a class errado ou certo quando o usuário tenta editar o campo
    jQuery(".calculadora-form input").keyup(function(){
        
        
        jQuery(this).removeClass("certo");
        
        jQuery(this).removeClass("errado");
        
    });
    
    //Retira a class errado ou certo quando o usuário tenta mudar o select
    jQuery(".calculadora-form select").on("click", function() {
                                                               
                                                               
        jQuery(this).removeClass("certo");
        
        jQuery(this).removeClass("errado");

    });

    //Ativa ou Desativa o Campo Qtd de Portas
    jQuery( ".calculadora-form .possui-portas" ).change(function() {

        parent = jQuery( this ).parents(".calculadora-form");

        if(jQuery( this ).val() == "sim"){

            jQuery( parent ).addClass( "portas-ativo" );

        }else{

            jQuery( parent ).removeClass( "portas-ativo" );

        }

    });

        //Ativa ou Desativa o Campo Qtd de Janelas
        jQuery( ".calculadora-form .possui-janelas" ).change(function() {

            parent = jQuery( this ).parents(".calculadora-form");
    
            if(jQuery( this ).val() == "sim"){
    
                jQuery( parent ).addClass( "janelas-ativo" );
    
            }else{
    
                jQuery( parent ).removeClass( "janelas-ativo" );
    
            }
    
        });

    //Começa as Validações                                    
    jQuery(".calculadora-form button").on("click", function() {

        //Pega em Qual passo o Usuário esta
        passo = jQuery(this).attr('class');

        //Caso não seja o ultimo passo pega o próximo passo
        if(passo != "calculadora-passo-4"){

            numero_passo = passo.replace(/[^\d]/g, "");

            numero_passo = parseInt(numero_passo) + 1;

            proximo_passo = "calculadora-passo-"+numero_passo;

        }

        //Quando true esta variavel impede de ir para o proximo passo
        erro = false;

        //Array para alocar todos os Erros
        erros = new Array();

        if(passo != "calculadora-passo-0"){

            jQuery("#"+passo+" select").addClass("certo");

            //Pega os Valores dos Campos do Passo em Questão
            altura_parede = parseFloat(jQuery('#'+passo+' .altura-parede').val());

            console.log(altura_parede);

            largura_parede = parseFloat(jQuery('#'+passo+' .largura-parede').val());

            qtd_portas = 0;

            qtd_janelas = 0;

            area_portas_total = 0;

            area_janelas_total = 0;

            possui_portas = jQuery('#'+passo+' .possui-portas').val();

            if(possui_portas == "sim"){

                qtd_portas =  parseFloat(jQuery('#'+passo+' .qtd-portas').val());

            }

            possui_janelas = jQuery('#'+passo+' .possui-janelas').val();

            if(possui_janelas == "sim"){

                qtd_janelas =  parseFloat(jQuery('#'+passo+' .qtd-janelas').val());
                
            }

            //Valida se o Campo Altura esta correto
            if(!!altura_parede){

                if(possui_portas == "sim"){

                    if(altura_parede < 2.2){

                        jQuery("#"+passo+" .altura-parede").removeClass("certo");

                        jQuery("#"+passo+" .altura-parede").addClass("errado");

                        erro = true;

                        erros.push("A Altura da parede não pode ser menor que 2,20m!");

                    }else if(altura_parede > 50){

                        jQuery("#"+passo+" .altura-parede").removeClass("certo");

                        jQuery("#"+passo+" .altura-parede").addClass("errado");

                        erro = true;

                        erros.push("A Altura da parede não pode ser mais que 50m!");

                    }else{

                        jQuery("#"+passo+" .altura-parede").removeClass("errado");

                        jQuery("#"+passo+" .altura-parede").addClass("certo");

                    }

                }else{

                    jQuery("#"+passo+" .altura-parede").removeClass("errado");

                    jQuery("#"+passo+" .altura-parede").addClass("certo");
                }

            }else{

                jQuery("#"+passo+" .altura-parede").removeClass("certo");

                jQuery("#"+passo+" .altura-parede").addClass("errado");

                erro = true;

                erros.push("Campo Altura da Parede esta em Branco!");

            }

            //Valida se o Campo Largura esta correto
            if(!!largura_parede){

                if(largura_parede < 1){

                    jQuery("#"+passo+" .largura-parede").removeClass("certo");

                    jQuery("#"+passo+" .largura-parede").addClass("errado");

                    erro = true;

                    erros.push("A Largura da parede não pode ser Menor que 1m!");
                    
                }else if(largura_parede > 50){

                    jQuery("#"+passo+" .largura-parede").removeClass("certo");

                    jQuery("#"+passo+" .largura-parede").addClass("errado");

                    erro = true;

                    erros.push("A Largura da parede não pode ser Maior que 50m!");
                }else{

                    jQuery("#"+passo+" .largura-parede").removeClass("errado");

                    jQuery("#"+passo+" .largura-parede").addClass("certo");

                }

            }else{

                jQuery("#"+passo+" .largura-parede").removeClass("certo");

                jQuery("#"+passo+" .largura-parede").addClass("errado");

                erro = true;

                erros.push("Campo Largura da Parede esta em Branco!");

            }


            //Valida o Campo Quantidade de Portas
            if(possui_portas == "sim"){

                if(!!qtd_portas){

                    area_porta = 0.8 * 1.9;

                    area_portas_total = area_porta * qtd_portas;


                }else{

                    jQuery("#"+passo+" .qtd-portas").removeClass("certo");

                    jQuery("#"+passo+" .qtd-portas").addClass("errado");

                    erro = true;

                    erros.push("Campo Quantidade de Portas esta em Branco!");

                }

            }

            //Valida o Campo Quantidade de Janelas
            if(possui_janelas == "sim"){

                if(!!qtd_janelas){

                    area_janela = 2.0 * 1.2;

                    area_janelas_total = area_janela * qtd_janelas;


                }else{

                    jQuery("#"+passo+" .qtd-janelas").removeClass("certo");

                    jQuery("#"+passo+" .qtd-janelas").addClass("errado");

                    erro = true;

                    erros.push("Campo Quantidade de Janelas esta em Branco!");

                }

            }
            
            //Verifica se Area das Portas e Janelas é no 50% da area da Parede
            if(area_janelas_total > 0 || area_portas_total > 0){
                
                area_parede = largura_parede * altura_parede;

                area_parede_metade = area_parede / 2;

                area_portas_janelas = area_janelas_total + area_portas_total;

                if(area_portas_janelas < area_parede_metade){

                    jQuery("#"+passo+" .qtd-portas").removeClass("errado");

                    jQuery("#"+passo+" .qtd-portas").addClass("certo");

                    jQuery("#"+passo+" .qtd-janelas").removeClass("errado");

                    jQuery("#"+passo+" .qtd-janelas").addClass("certo");

                }else{

                    jQuery("#"+passo+" .qtd-portas").removeClass("certo");

                    jQuery("#"+passo+" .qtd-portas").addClass("errado");

                    jQuery("#"+passo+" .qtd-janelas").removeClass("certo");

                    jQuery("#"+passo+" .qtd-janelas").addClass("errado");

                    erro = true;

                    erros.push("A area das Janelas + Portas é maior que 50% da area da Parede");

                }

            }

        }


        //Se algum Campo Estiver com Erro são apresentados os erros na tela e não vai para o proximo passo
        //Se não, vai para o proximo passo ou é dado o submit no form
        if (erro) {
            
            jQuery('html, body').animate({
                 scrollTop: jQuery("body").offset().top
             }, 1500);

             erros_texto = "<p>"+erros.join("<br/>")+"</p>";

             jQuery( ".form-erros" ).html(erros_texto);

        } else {

            if(passo == "calculadora-passo-4"){

                jQuery( "#calculadora-form-submit" ).submit();

            }
            
            jQuery('html, body').animate({
                 scrollTop: jQuery("body").offset().top
             }, 1500);

            jQuery( ".form-erros" ).html("");

            jQuery("#"+passo+" input").removeClass("errado");

            jQuery("#"+passo+" input").addClass("certo");
            
            jQuery(".calcularora-passos span").removeClass("ativo");
            
            jQuery(".calcularora-passos span."+proximo_passo).addClass("ativo");

            jQuery("#"+passo).removeClass("ativo");

            setTimeout(function() {

                jQuery("#"+proximo_passo).addClass("ativo");

            }, 510);

        }

    });

});