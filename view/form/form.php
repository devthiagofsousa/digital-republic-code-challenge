<section class="calculadora">

    <div class="calculadora-container">

        <h1>Calculadora de Quantidade de Tinta</h1>

        <div class="calcularora-passos">

            <span class="calculadora-passo-1">1</span>
            <span class="calculadora-passo-2">2</span>
            <span class="calculadora-passo-3">3</span>
            <span class="calculadora-passo-4">4</span>

        </div>

        <div class="form-erros">
            
        </div>

        <form id="calculadora-form-submit" method="post" action="<?php echo url(); ?>controller/controller.php">

            <div id="calculadora-passo-0" class="calculadora-form ativo">

            <!-- Apresentação -->

                <p>Esta aplicação tem intuito de ajudar pintores a saber a quantidade de tinta necessaria para se pintar uma sala, quantidade de latas e litros de cada lata</p>

                <p><b>Observações:</b> 

                    <ul>

                        <li>Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes</li>

                        <li>O total de área das portas e janelas deve ser no máximo 50% da área de parede</li>

                        <li>A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta</li>

                        <li>Cada janela possui as medidas: 2,00 x 1,20m</li>

                        <li>Cada porta possui as medidas: 0,80 x 1,90</li>

                        <li>Cada litro de tinta é capaz de pintar 5 metros quadrados</li>

                        <li>Não considerar teto nem piso.</li>

                    </ul>

                </p>

                <button class="calculadora-passo-0" type="button">Vamos lá?</button>

            </div>

            <div id="calculadora-passo-1" class="calculadora-form">

                <!-- Passo 01 -> Informar Tamanho da Parede 01 -->
                
                <h2>Passo 01</h2>

                <div class="calculadora-campos">

                    <p>
                        <label>
                            Informe a Altura em Metros da Primeira Parede:
                            <input class="altura-parede" name="altura-parede-01" type="number" />
                        </label>

                        <label>
                            Informe a Largura em Metros da Primeira Parede:
                            <input class="largura-parede" name="largura-parede-01" type="number" />
                        </label>
                    </p>

                    <p>
                        <label>
                            Esta Parede possui portas?
                            <select class="possui-portas" name="possui-portas-01">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-paredes-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Portas:
                                <input class="qtd-portas" name="qtd-portas-01" type="number" />
                            </label>
                        </p>
                    </div>

                    <p>
                        <label>
                            Esta Parede possui Janelas?
                            <select class="possui-janelas" name="possui-janelas-01">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-janelas-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Janelas:
                                <input class="qtd-janelas" name="qtd-janelas-01" type="number" />
                            </label>
                        </p>
                    </div>

                </div>

                <button class="calculadora-passo-1" type="button">Continuar</button>

            </div>

            <div id="calculadora-passo-2" class="calculadora-form">

                <!-- Passo 02 -> Informar Tamanho da Parede 02 -->

                <h2>Passo 02</h2>

                <div class="calculadora-campos">

                    <p>
                        <label>
                            Informe a Altura em Metros da Segunda Parede:
                            <input class="altura-parede" name="altura-parede-02" type="number" />
                        </label>

                        <label>
                            Informe a Largura em Metros da Segunda Parede:
                            <input class="largura-parede" name="largura-parede-02" type="number" />
                        </label>
                    </p>

                    <p>
                        <label>
                            Esta Parede possui portas?
                            <select class="possui-portas" name="possui-portas-02">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-paredes-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Portas:
                                <input class="qtd-portas" name="qtd-portas-02" type="number" />
                            </label>
                        </p>
                    </div>

                    <p>
                        <label>
                            Esta Parede possui Janelas?
                            <select class="possui-janelas" name="possui-janelas-02">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-janelas-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Janelas:
                                <input class="qtd-janelas" name="qtd-janelas-02" type="number" />
                            </label>
                        </p>
                    </div>

                </div>

                    <button class="calculadora-passo-2" type="button">Continuar</button>

                </div>

                <div id="calculadora-passo-3" class="calculadora-form">

                <!-- Passo 03 -> Informar Tamanho da Parede 03 -->

                <h2>Passo 03</h2>

                <div class="calculadora-campos">

                    <p>
                        <label>
                            Informe a Altura em Metros da Terceira Parede:
                            <input class="altura-parede" name="altura-parede-03" type="number" />
                        </label>

                        <label>
                            Informe a Largura em Metros da Terceira Parede:
                            <input class="largura-parede" name="largura-parede-03" type="number" />
                        </label>
                    </p>

                    <p>
                        <label>
                            Esta Parede possui portas?
                            <select class="possui-portas" name="possui-portas-03">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-paredes-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Portas:
                                <input class="qtd-portas" name="qtd-portas-03" type="number" />
                            </label>
                        </p>
                    </div>

                    <p>
                        <label>
                            Esta Parede possui Janelas?
                            <select class="possui-janelas" name="possui-janelas-03">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-janelas-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Janelas:
                                <input class="qtd-janelas" name="qtd-janelas-03" type="number" />
                            </label>
                        </p>
                    </div>

                </div>

                    <button class="calculadora-passo-3" type="button">Continuar</button>

                </div>

                <div id="calculadora-passo-4" class="calculadora-form">

                <!-- Passo 04 -> Informar Tamanho da Parede 04 -->

                <h2>Passo 04</h2>

                <div class="calculadora-campos">

                    <p>
                        <label>
                            Informe a Altura em Metros da Quarta Parede:
                            <input class="altura-parede" name="altura-parede-04" type="number" />
                        </label>

                        <label>
                            Informe a Largura em Metros da Quarta Parede:
                            <input class="largura-parede" name="largura-parede-04" type="number" />
                        </label>
                    </p>

                    <p>
                        <label>
                            Esta Parede possui portas?
                            <select class="possui-portas" name="possui-portas-04">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-paredes-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Portas:
                                <input class="qtd-portas" name="qtd-portas-04" type="number" />
                            </label>
                        </p>
                    </div>

                    <p>
                        <label>
                            Esta Parede possui Janelas?
                            <select class="possui-janelas" name="possui-janelas-04">

                                <option value="nao" selected >Não</option>

                                <option value="sim">Sim</option>

                            </select>
                        </label>
                    </p>

                    <div class="possui-janelas-sim">
                        <p>
                            <label>
                                Informe a Quantidade de Janelas:
                                <input class="qtd-janelas" name="qtd-janelas-04" type="number" />
                            </label>
                        </p>
                    </div>

                </div>

                <button class="calculadora-passo-4" type="button">Ver Resultados</button>

                </div>
            
        </form>

    </div>

</section>
